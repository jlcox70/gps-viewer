// axiosConfig.js
import axios from 'axios';


const baseURL = (() => {
  const hostname = window.location.hostname;
  const proto = window.location.protocol;
  const port = window.location.port;
  return `${proto}//${hostname}:${port}/api/gps`;
})();


const instance = axios.create({
  baseURL,
  // other Axios configurations...
});

export default instance;
