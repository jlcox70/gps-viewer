app := gps-web
app_version := 0.0.1

.PHONY: docker_local

docker:
	rm -fr node_modules*
	docker run --rm --net host  -ti -v `pwd`:/project:z -w /project  node bash

install: setup
	npm install
setup:
	npm config set strict-ssl false
	npm config set registry "http://registry.npmjs.org/"
	yarn config set strict-ssl false
	yarn config set registry "http://registry.npmjs.org/"
run: install
	npm run serve

docker_local:
	docker build -t $(app):$(app_version) 	. --load
docker_run: docker_local
	docker run --net host -ti --rm gps-web:0.0.1

docker_build:
	docker buildx build --platform linux/amd64,linux/arm64  -t $(app):$(app_version) . 

pushx:
	docker buildx build --platform linux/amd64,linux/arm64  -t $(app):$(app_version). --push