const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  
  devServer: {
    port: 80,
  },
  // configureWebpack: {
  //   resolve: {
  //     extensions: ['.js', '.json', '.yaml', '.yml'],
  //   },
  // },
  // chainWebpack: config => {
  //   // Add raw-loader for YAML files
  //   config.module
  //     .rule('yaml')
  //     .test(/\.ya?ml$/)
  //     .use('raw-loader')
  //     .loader('raw-loader')
  //     .end();
  // },
};
