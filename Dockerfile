
FROM node as builder
WORKDIR /app
COPY . .
RUN make install 
RUN yarn build


FROM node:14-alpine
WORKDIR /app
COPY --from=builder /app/dist/ .
COPY http-server/package*.json ./
COPY ./Makefile .
RUN apk add make && make install && npm install -g http-server

ENTRYPOINT [ "http-server" ]