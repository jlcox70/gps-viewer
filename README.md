# gps-viewer

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

\

npm config set strict-ssl false
npm config set registry "http://registry.npmjs.org/"
npm config set proxy http://192.168.1.80:6101/